import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

const SERVER='http://localhost:3000';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    empleados: [],
    clientes: [],
    loaded: false,
  },
  getters: {
    getClientesById: (state) => (id) => {
      return state.clientes.find(cliente => cliente.gestor === id)
    }
  },
  mutations: {
    setEmpleados(state, empleados) {
      state.empleados=empleados;
  },
  setClientes(state, clientes) {
    state.clientes=clientes;
},

setState(state, value) {
          state.loaded=value;
      },
      delEmpleado(state, id) {
        state.empleados=state.empleados.filter(empleado=>empleado.id!=id);
      },
      addCliente(state, cliente) {
        state.clientes.push(cliente);
      },
      modifyCliente(state, cliente) {
        let index=state.clientes.findIndex(item=>item.id==cliente.id);
        state.clientes.splice(index, 1, cliente);
      },
  },
  actions: {
      loadData(context) {
          if (!context.state.loaded) {
            axios.get(SERVER+'/empleados')
            .then(response=>{
                context.commit('setEmpleados', response.data);
                context.commit('setState', true);
            }).catch(err=>alert('ERROR: '+err.message||err))
    
            axios.get(SERVER+'/clientes')
            .then(response=>{
                context.commit('setClientes', response.data);
                context.commit('setState', true);
            }).catch(err=>alert('ERROR: '+err.message||err))

          }
      },
      borrarEmpleado(context, id) {
        axios.delete(SERVER+'/empleados/'+id)
        .then(()=>context.commit('delEmpleado', id))
        .catch(err=>alert('ERROR: '+err.message||err))       

      },
      addGrupo(context, grupo) {
        axios.post(SERVER+'/grupos', grupo)
        .then(response=>context.commit('addGrupo', response.data))
        .catch(err=>alert('ERROR: '+err.message||err))
      },
      modifyGrupo(context, grupo) {
        axios.put(SERVER+'/grupos/'+grupo.id, grupo)
        .then(response=>context.commit('modifyGrupo', response.data))
        .catch(err=>alert('ERROR: '+err.message||err))
      },
  }
})
export default store
