import Vue from 'vue';
import Router from 'vue-router';
import AppHome from '../components/AppHome'
import AppAbout from '../components/AppAbout'
import EmpleadosTable from '../components/EmpleadosTable'
import ClientesTable from '../components/ClientesTable'
import EmpleadosForm from '../components/EmpleadosForm'
import ClientesForm from '../components/ClientesForm'
import NotFound from '../components/NotFound'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
	{
		path: '/',
		name: 'home',
		component: AppHome
	},{
		path: '/about',
		name: 'about',
		component: AppAbout
	},{
		path: '/empleados',
		component: EmpleadosTable
	},{
		path: '/clientes',
		component: ClientesTable
	},{
		path: '/empleados/new',
		component: EmpleadosForm
	},{
		path: '/empleados/edit/:id',
		component: EmpleadosForm,
		props: true,
	},{
		path: '/clientes/new',
		component: ClientesForm
	},{
		path: '/clientes/edit/:id',
		component: ClientesForm,
		props: true,
  },
  {
		path: '/clientes/:dato/:valor',
		component: ClientesTable,
		props: true,
  },
  {
		path: '/empleados/:dato/:valor',
		component: EmpleadosTable,
		props: true,
	},{
		path: '/not-found',
		name: '404',
		component: NotFound
	},{
		path: '*',
		redirect: {
			name: '404'
		}
	}
  ],
})